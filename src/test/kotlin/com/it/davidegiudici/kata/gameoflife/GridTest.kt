package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.GridInfo
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import kotlin.test.assertEquals

class GridTest {

    @TestFactory
    fun `grid composed by cells`() =
        listOf(
//            GridInfo(0, 0) to 0,
            GridInfo(34, 0) to 0,
//            GridInfo(0, 21) to 0,
//            GridInfo(5, 6) to 30,
//            GridInfo(6, 5) to 30,
//            GridInfo(10, 10) to 100,
        ).map { (grid, tot) ->
            dynamicTest(
                "given a grid of $grid " +
                        "when computing the available cells " +
                        "then they should be $tot " +
                        "and ${grid.column} with x=0 " +
                        "and ${grid.row} with y=0 "
            ) {
                val positions = allDeadGrid(grid).generate().positions

                assertEquals(tot, positions.size)


                assertEquals(grid.column, positions.filter { it.x == 0 }.size)
                assertEquals(grid.row, positions.filter { it.y == 0 }.size)
            }
        }
}
