package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.GameOfLife
import com.it.davidegiudici.kata.gameoflife.domain.GridInfo
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class GameOfLifeTest {

    @Test
    fun `gol run for a maximum amount of runs`() {
        val numOfGens = 5
        val gr = allDeadGrid(GridInfo(5,5)).generate()
        val gol = GameOfLife(gr, 100, numOfGens)
        var counter = 0
        gol.run( { println("gen"); counter++ })

        assertEquals(numOfGens, counter)
    }
}