package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.Grid
import com.it.davidegiudici.kata.gameoflife.domain.GridInfo
import com.it.davidegiudici.kata.gameoflife.doors.outdoor.console.GridToConsole
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.test.assertEquals

private val NL: String = System.lineSeparator()!!

class GridToConsoleTest {

    @TestFactory
    fun `print dead grid composed by cells`() =
        listOf(
            GridInfo(0, 0) to "",
            GridInfo(34, 0) to "",
            GridInfo(0, 21) to "",
            GridInfo(1, 6) to ".  .  .  .  .  .",
            GridInfo(6, 1) to ".$NL.$NL.$NL.$NL.$NL.",
            GridInfo(3, 1) to ".$NL.$NL.",
        ).map { (grid, result) ->

            DynamicTest.dynamicTest(
                "given an allDead grid of $grid" +
                        "when printing all the available cells " +
                        "then it should be reported as $result"
            ) {
                serializeAndAssert(result, allDeadGrid(grid).generate())
            }
        }

    @TestFactory
    fun `print alive grid composed by cells`() =
        listOf(
            GridInfo(1, 6) to "*  *  *  *  *  *",
            GridInfo(6, 1) to "*$NL*$NL*$NL*$NL*$NL*",
            GridInfo(3, 1) to "*$NL*$NL*",
        ).map { (grid, result) ->

            DynamicTest.dynamicTest(
                "given an allAlive grid of $grid" +
                        "when printing all the available cells " +
                        "then it should be reported as $result"
            ) {
                serializeAndAssert(result, allAliveGrid(grid).generate())
            }
        }

    private fun serializeAndAssert(expected: String, grid: Grid) {
        val serialized = collectStdOut {
            GridToConsole().serialize(grid)
        }
        assertEquals(expected, serialized)
        println(expected)
    }
}

private fun collectStdOut(f: () -> Unit): String {
    val storeForLater = System.out
    return ByteArrayOutputStream().use {
        System.setOut(PrintStream(it))
        f()
        System.setOut(storeForLater)
        it.toString().dropLast(NL.length)
    }
}
