package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.GridInfo
import com.it.davidegiudici.kata.gameoflife.domain.NeighborPositionFinder
import com.it.davidegiudici.kata.gameoflife.domain.Position
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import kotlin.test.assertEquals

class NeighborPositionFinderTest {

    @TestFactory
    fun `neighbor from corners`() =
        listOf(
            GridInfo(0, 0) to 0,
            GridInfo(0, 21) to 0,
            GridInfo(21, 0) to 0,
            GridInfo(1, 5) to 1,
            GridInfo(5, 1) to 1,
            GridInfo(5, 6) to 3,
            GridInfo(6, 6) to 3,
            GridInfo(4, 4) to 3,
        ).map { (grid, resultSize) ->

            DynamicTest.dynamicTest(
                "given a grid of $grid" +
                        "when getting the neighbor of all the corners " +
                        "then it should get $resultSize neighbors"
            ) {
                val neighborFinder = NeighborPositionFinder(grid)

                findCorner(grid).forEach {
                    val neighbor = neighborFinder.neighbor(it)
                    assertEquals(resultSize, neighbor.size)
                }
            }
        }

    @TestFactory
    fun `neighbor from edges`() =
        listOf(
            GridInfo(0, 0) to 0,
            GridInfo(0, 21) to 0,
            GridInfo(21, 0) to 0,
            GridInfo(1, 5) to 2,
            GridInfo(5, 1) to 2,
            GridInfo(5, 6) to 5,
            GridInfo(6, 6) to 5,
            GridInfo(4, 4) to 5,
        ).map { (grid, resultSize) ->

            DynamicTest.dynamicTest(
                "given a grid of $grid" +
                        "when getting the neighbor of all the corners " +
                        "then it should get $resultSize neighbors"
            ) {
                val neighborFinder = NeighborPositionFinder(grid)

                findEdge(grid).forEach {
                    val neighbor = neighborFinder.neighbor(it)
                    assertEquals(resultSize, neighbor.size)
                }
            }
        }

    @TestFactory
    fun `neighbor from middle`() =
        listOf(
            GridInfo(5, 6) to 8,
            GridInfo(6, 6) to 8,
            GridInfo(4, 4) to 8,
        ).map { (grid, resultSize) ->

            DynamicTest.dynamicTest(
                "given a grid of $grid" +
                        "when getting the neighbor of all the corners " +
                        "then it should get $resultSize neighbors"
            ) {
                val neighborFinder = NeighborPositionFinder(grid)

                findMiddle(grid).forEach {
                    val neighbor = neighborFinder.neighbor(it)
                    assertEquals(resultSize, neighbor.size)
                }
            }
        }

    private fun findEdge(info: GridInfo): Set<Position> {
        val edgeSet = mutableSetOf<Position>()

        if (info.row > 0 && info.column > 1) {
            val topY = 0
            val bottomY = info.row - 1

            val minX = 1
            val maxX = info.column - 2

            // top
            edgeSet.add(Position((minX..maxX).random(), topY).apply { println("top $this") })
            // bottom
            edgeSet.add(Position((minX..maxX).random(), bottomY).apply { println("bottom $this") })
        }

        if (info.row > 1 && info.column > 0) {

            val leftX = 0
            val rightX = info.column - 1

            val minY = 1
            val maxY = info.row - 2

            // left
            edgeSet.add(Position(leftX, (minY..maxY).random()).apply { println("left $this") })

            // right
            edgeSet.add(Position(rightX, (minY..maxY).random()).apply { println("right $this") })
        }

        return edgeSet
    }

    private fun findMiddle(info: GridInfo): Set<Position> {

        val minY = 1
        val maxY = info.row - 2

        val minX = 1
        val maxX = info.column - 2

        val middleSet = mutableSetOf<Position>()

        if (info.row > 2 && info.column > 2) {

            (0..5).forEach { _ ->
                middleSet.add(Position((minX..maxX).random(), (minY..maxY).random()))
            }

        }

        return middleSet
    }

    private fun findCorner(info: GridInfo): Set<Position> {
        val cornerSet = mutableSetOf<Position>()

        val topY = 0
        val bottomY = info.row - 1

        val leftX = 0
        val rightX = info.column - 1

        // Top left
        cornerSet.add(Position(leftX, topY).apply { println("Top left $this") })

        // Top right
        cornerSet.add(Position(rightX, topY).apply { println("Top right $this") })

        // Bottom left
        cornerSet.add(Position(leftX, bottomY).apply { println("Bottom left $this") })

        // Bottom right
        cornerSet.add(Position(rightX, bottomY).apply { println("Bottom right $this") })

        return cornerSet
    }
}
