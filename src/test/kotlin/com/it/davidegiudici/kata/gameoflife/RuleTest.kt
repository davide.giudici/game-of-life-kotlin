package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.Cell
import com.it.davidegiudici.kata.gameoflife.domain.Rules
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

class RuleTest {
    @TestFactory
    fun `next generation on Dead cell`() =
        listOf(
            0 to Cell.Dead,
            1 to Cell.Dead,
            2 to Cell.Dead,
            3 to Cell.Alive,
            4 to Cell.Dead,
            5 to Cell.Dead,
            6 to Cell.Dead,
            7 to Cell.Dead,
            8 to Cell.Dead,
        ).map { (neighbor, resultCell) ->

            DynamicTest.dynamicTest(
                "given a Dead cell with $neighbor neighbor " +
                        "when going to the next generation " +
                        "then the cell should become $resultCell"
            ) {
                val rules = Rules()
                val newCell = rules.apply(Cell.Dead, neighbor)

                assertEquals(resultCell, newCell)
            }
        }

    @Test
    fun `cannot have more than 8 neighbor`() {
        val rules = Rules()
        (9..20).forEach { neighbor ->
            assertThrows<IllegalStateException> {
                rules.apply(Cell.Dead, neighbor)
            }
        }
    }

    @TestFactory
    fun `next generation on Alive cell`() =
        listOf(
            0 to Cell.Dead,
            1 to Cell.Dead,
            2 to Cell.Alive,
            3 to Cell.Alive,
            4 to Cell.Dead,
            5 to Cell.Dead,
            6 to Cell.Dead,
            7 to Cell.Dead,
            8 to Cell.Dead,
        ).map { (neighbor, resultCell) ->

            DynamicTest.dynamicTest(
                "given an Alive cell with $neighbor neighbor " +
                        "when going to the next generation " +
                        "then the cell should become $resultCell"
            ) {
                val rules = Rules()
                val newCell = rules.apply(Cell.Alive, neighbor)

                assertEquals(resultCell, newCell)
            }
        }
}
