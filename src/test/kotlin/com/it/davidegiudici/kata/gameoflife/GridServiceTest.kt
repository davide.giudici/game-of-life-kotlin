package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.*
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import kotlin.test.assertEquals

fun generateAGrid(info: GridInfo, status: Cell) =
    GridGenerator { Grid(info) { status } }

fun allDeadGrid(info: GridInfo) = generateAGrid(info, Cell.Dead)
fun allAliveGrid(info: GridInfo) = generateAGrid(info, Cell.Alive)

private val D = Cell.Dead
private val A = Cell.Alive

class GridServiceTest {

    @TestFactory
    fun `single line grid all Alive, result on dead corners`() =
        listOf(
            1 to Pair(listOf(D), listOf(D)),
            2 to Pair(listOf(D, D), listOf(D, D)),
            3 to Pair(listOf(D, A, D), listOf(D, D, D)),
            7 to Pair(listOf(D, A, A, A, A, A, D), listOf(D,D,A,A,A,D,D)),
        ).map { (column, expectedResult) ->

            DynamicTest.dynamicTest(
                "given an grid with 1 row and $column column " +
                         "when going to the next generations " +
                        "then the cells should become ${expectedResult.first} and then ${expectedResult.second}"
            ) {
                assertThreeGens(expectedResult, allAliveGrid(GridInfo(1, column)))
            }
        }

    @TestFactory
    fun `single column grid all Alive, result on dead corners`() =
        listOf(
            1 to Pair(listOf(D), listOf(D)),
            2 to Pair(listOf(D,D), listOf(D,D)),
            3 to Pair(listOf(D,A,D), listOf(D,D,D)),
            7 to Pair(listOf(D,A,A,A,A,A,D), listOf(D,D,A,A,A,D,D)),
        ).map { (row, expectedResult) ->

            DynamicTest.dynamicTest(
                "given an grid with 1 column and $row row " +
                        "when going to the next generations " +
                        "then the cells should become ${expectedResult.first} and then ${expectedResult.second}"
            ) {
                assertThreeGens(expectedResult, allAliveGrid(GridInfo(row, 1)))
            }
        }

    private fun assertThreeGens(expectedResult: Pair<List<Cell>, List<Cell>>, gridGenerator: GridGenerator) {

        val service = GridService()
        val gen0 = gridGenerator.generate()

        val gen1 = service.nextGeneration(gen0)
        gen1.positions.forEachIndexed { i, pos ->
            assertEquals(expectedResult.first[i], gen1.getCell(pos))
        }

        val gen2 = service.nextGeneration(gen1)
        gen2.positions.forEachIndexed { i, pos ->
            assertEquals(expectedResult.second[i], gen2.getCell(pos))
        }
    }
}
