package com.it.davidegiudici.kata.gameoflife.domain

class NeighborPositionFinder(private val gridInfo: GridInfo) {

    fun neighbor(position: Position) = getNeighborPositionOrNull(position).filterNotNull()

    private fun getNeighborPositionOrNull(position: Position) =
        getAboveNeighbors(position) +
        getLeftNeighbor(position) +
        getRightNeighbor(position) +
        getBelowNeighbors(position)

    private fun getBelowNeighbors(position: Position) = getLineNeighbors(position, position.y + 1)
    private fun getAboveNeighbors(position: Position) = getLineNeighbors(position, position.y - 1)
    private fun getLineNeighbors(position: Position, y: Int): List<Position?> {
        val xLeft = position.x - 1
        val xRight = position.x + 1

        return listOf(
            getCellOrNull(position.copy(y = y, x = xLeft)),// 1
            getCellOrNull(position.copy(y = y)), // 2
            getCellOrNull(position.copy(y = y, x = xRight)) // 3
        )
    }

    private fun getRightNeighbor(position: Position) = getSameRowNeighbor(position, position.x + 1)
    private fun getLeftNeighbor(position: Position) = getSameRowNeighbor(position, position.x - 1)
    private fun getSameRowNeighbor(position: Position, x: Int) = getCellOrNull(position.copy(x = x))

    private fun getCellOrNull(position: Position) =
        when {
            position.x < 0 -> null
            position.y < 0 -> null
            position.x > gridInfo.maxX -> null
            position.y > gridInfo.maxY -> null
            else -> position
        }

}
