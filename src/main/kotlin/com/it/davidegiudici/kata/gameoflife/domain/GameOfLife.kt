package com.it.davidegiudici.kata.gameoflife.domain

class GameOfLife(
    initialGrid: Grid,
    private val timingMs: Long,
    private val iterations: Int? = null
) {
    private var grid = initialGrid
    private val service = GridService()

    fun run(vararg handler: GridSerializer) {
        var cnt = 0
        while(true) {
            cnt++
            cycle(handler.toList())
            if (iterations != null && cnt >= iterations)
                break
        }
    }

    private fun cycle(handler: List<GridSerializer>) {
        handler.forEach {  it.serialize(grid) }
        grid = service.nextGeneration(grid)
        Thread.sleep(timingMs)
    }
}
