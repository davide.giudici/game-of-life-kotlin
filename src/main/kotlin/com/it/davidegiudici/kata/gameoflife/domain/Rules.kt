package com.it.davidegiudici.kata.gameoflife.domain

class Rules {
    fun apply(cell: Cell, neighbor: Int) =
        when (neighbor) {
            in (0..1) -> Cell.Dead
            in (4..8) -> Cell.Dead
            2 -> cell
            3 -> Cell.Alive
            else -> throw IllegalStateException("In 2D grid, a cell can't have more than 8 neighbor")
        }
}
