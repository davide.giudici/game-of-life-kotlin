package com.it.davidegiudici.kata.gameoflife.domain

class GridInfo(rowArg: Int, columnArg: Int) {

    private val isNotEmpty = (rowArg > 0 && columnArg > 0)
    private fun valueOrZero(value: Int) = if (isNotEmpty) value else 0

    val row: Int = valueOrZero(rowArg)
    val column: Int = valueOrZero(columnArg)

    val maxX = column - 1
    val maxY = row - 1
    
    override fun toString() = "Grid $row X $column"
}
