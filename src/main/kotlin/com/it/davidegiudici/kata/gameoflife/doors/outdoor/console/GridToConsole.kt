package com.it.davidegiudici.kata.gameoflife.doors.outdoor.console

import com.it.davidegiudici.kata.gameoflife.domain.Cell
import com.it.davidegiudici.kata.gameoflife.domain.Grid
import com.it.davidegiudici.kata.gameoflife.domain.GridSerializer

class GridToConsole(
    private val alive: String = "*",
    private val dead: String = ".",
) : GridSerializer {

    private val LINE_SPACE = "  "
    private val NEW_LINE = System.lineSeparator()

    override fun serialize(grid: Grid) =
        println(serializeToStr(grid))

    private fun serializeToStr(grid: Grid) =
        (0..grid.info.maxY)
            .map { row ->
                grid.positions
                    .filter { it.y == row }
                    .joinToString(LINE_SPACE) {
                        grid.getCell(it)
                            .let(::printCell)
                    }
            }.filter { it.isNotBlank() }
            .joinToString(NEW_LINE) { it }

    private fun printCell(cell: Cell): String =
        when (cell) {
            Cell.Dead -> dead
            Cell.Alive -> alive
        }
}
