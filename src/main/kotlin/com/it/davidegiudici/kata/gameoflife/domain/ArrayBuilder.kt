package com.it.davidegiudici.kata.gameoflife.domain

class ArrayBuilder(private val info: GridInfo) {
    fun builder(cellBuilder: (position: Position) -> Cell) =
        Array(info.column) { x -> (Array(info.row) { y -> cellBuilder(Position(x, y)) }) }
}
