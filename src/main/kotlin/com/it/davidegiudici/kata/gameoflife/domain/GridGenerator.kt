package com.it.davidegiudici.kata.gameoflife.domain

fun interface GridGenerator {
    fun generate(): Grid
}
