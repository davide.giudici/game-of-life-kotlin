package com.it.davidegiudici.kata.gameoflife.doors.outdoor.processing

import com.it.davidegiudici.kata.gameoflife.domain.Cell
import com.it.davidegiudici.kata.gameoflife.domain.Grid
import com.it.davidegiudici.kata.gameoflife.domain.GridSerializer
import com.it.davidegiudici.kata.gameoflife.domain.Position
import processing.core.PApplet

class GridToProcessing(private val size: Int) : PApplet(), GridSerializer {

    private val cellSize = 10
    private val dead = color(0)
    private val alive = color(0, 200, 0)

    private var grid: Grid? = null

    @Synchronized
    override fun serialize(grid: Grid) {
        this.grid = grid
    }

    override fun settings() {
        size(size * cellSize, size * cellSize)
        noSmooth()
    }

    override fun setup() {
        frameRate(10F)
    }

    private fun Cell.draw(pos: Position) {
        fill(this.color())
        noStroke()
        rect(pos.x.toFloat() * cellSize, pos.y.toFloat() * cellSize, cellSize.toFloat(), cellSize.toFloat())
    }

    @Synchronized
    override fun draw() {
        grid?.let {
            background(dead)
            it.positions.forEach { pos ->
                it.getCell(pos).draw(pos)
            }
            grid = null
        }
    }

    private fun Cell.color() =
        when (this) {
            Cell.Alive -> alive
            Cell.Dead -> dead
        }
}
