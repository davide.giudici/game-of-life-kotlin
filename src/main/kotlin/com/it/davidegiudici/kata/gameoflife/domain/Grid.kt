package com.it.davidegiudici.kata.gameoflife.domain

class Grid(val info: GridInfo, cellBuilder: (position: Position) -> Cell) {

    private val data = ArrayBuilder(info).builder(cellBuilder)

    val positions: List<Position> = run {
        (0..info.maxX).map { x ->
            (0..info.maxY).map { y ->
                Position(x, y)
            }
        }.flatten()
    }

    fun getCell(pos: Position) = data[pos.x][pos.y]
}
