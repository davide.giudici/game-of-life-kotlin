package com.it.davidegiudici.kata.gameoflife.domain

fun interface GridSerializer {
    fun serialize(grid: Grid)
}
