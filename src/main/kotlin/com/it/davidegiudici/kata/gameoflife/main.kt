package com.it.davidegiudici.kata.gameoflife

import com.it.davidegiudici.kata.gameoflife.domain.*
import com.it.davidegiudici.kata.gameoflife.doors.outdoor.processing.GridToProcessing
import processing.core.PApplet
import kotlin.random.Random.Default.nextBoolean

fun main(args: Array<String>) {

    fun generateAGrid(square: Int) = GridGenerator {
        Grid(GridInfo(square, square)) { if (nextBoolean()) Cell.Alive else Cell.Dead }
    }

    val square = if (args.size > 0) args[0].toInt() else 11
    val iterations = if (args.size > 1) args[1].toInt() else 20
    val timing = if (args.size > 2) args[2].toLong() else 2000

    val graph = GridToProcessing(square)
    PApplet.runSketch(arrayOf("GameOfLive"), graph)

    val grid = generateAGrid(square).generate()
    GameOfLife(grid, timing, iterations).run(graph)
}
