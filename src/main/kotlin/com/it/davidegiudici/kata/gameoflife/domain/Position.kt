package com.it.davidegiudici.kata.gameoflife.domain

data class Position(val x: Int, val y: Int)
