package com.it.davidegiudici.kata.gameoflife.domain

sealed class Cell {
    object Alive : Cell() {
        override fun toString() = "Alive"
    }
    object Dead : Cell() {
        override fun toString() = "Dead"
    }
}
