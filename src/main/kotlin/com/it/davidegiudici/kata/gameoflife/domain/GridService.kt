package com.it.davidegiudici.kata.gameoflife.domain

class GridService {

    private val rules = Rules()

    fun nextGeneration(grid: Grid): Grid {
        return Grid(grid.info) {
            nextGenerationOfCell(grid, it)
        }
    }

    private fun nextGenerationOfCell(grid: Grid, position: Position): Cell {
        val neighborFinder = NeighborPositionFinder(grid.info)

        return grid.getCell(position).let { cell ->
            neighborFinder
                .neighbor(position)
                .map {
                    grid.getCell(it)
                }
                .filter { c -> c == Cell.Alive }
                .size
                .let { neighborNumber ->
                    rules.apply(cell, neighborNumber)
                }
        }
    }
}
